#Code B3

>"Drawing is a verb"
> [Richard Serra](https://www.moma.org/explore/inside_out/2011/10/20/to-collect/)

<a href="https://goo.gl/forms/8p7WP2cSN19wUvjh2" class="btn-inscriptions">Inscriptions</a>

Cette série de workshops s'adresse aux étudiants de B3, inscrits dans un CASO arts numériques. Ils auront lieu **les jeudis, de 14h à 18h** au 427 Av Louise, 1er étage (auditoire). (Venez avec votre machine)

Le but de cet ensemble de workshops est d'**approfondir le travail avec le code** rapidement évoqué en B1, afin d'enrichir vos projets, vous amenant à une **plus grande autonomie**, à la fois technique et artistique. 

Nous parlerons biens sûr de [Processing](https://www.processing.org/), mais nous travaillerons assez rapidement avec [P5js](https://p5js.org/), une bibliothèque javascript (en filiation directe) utilisant à très peu de choses près la même syntaxe, un des objectifs des workshops étant de **porter le code dans la page web**, pour faire se rejoindre workshops B1, cours de web B2 et CASO arts numériques B3, le tout **au service de votre travail artistique personnel**. 

Ceci nous permettra de dessiner dans la page web, d'interagir avec elle, mais aussi de manipuler le DOM ou de créer des oeuvres génératives récupérant des données en temps réel à travers des APIs (Application Programming Interface).

A noter que ces workshops se construiront également *en temps réel*, en fonction des projets de chacun, et se feront sous une **forme expérimentale et collaborative**.

Les différents projets qui en résulteront, idéalement en lien avec vos pratiques d'atelier, feront l'objet d'une exposition collective en ligne.


> Plus d'info sur [Processing & P5js](part0/processing-et-p5js.md)

<div class="notification" style="margin-top:4rem;border-left-color:#ff4747;">
    <h4>Pré-requis</h4>
    Il est nécessaire (bien que nous en reverrons les concepts de base) de se souvenir de ce qui a été vu en B2 concernant HTML et CSS. Nous en aurons besoin.
</div>



{{ "https://player.vimeo.com/video/74725118" | video }}


![](/assets/aymeraude.jpeg)
<span class="legende">Le travail d'Aymeraude, option Dessin, entièrement réalisé dans Illustrator.</span>
