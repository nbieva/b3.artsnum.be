#
Pour aller plus loin...
Dans le cas où vous voudriez aller plus loin, vous trouverez ci-dessous quelques propositions.

1. Créez un texte dont la taille varie en fonction du niveau de votre voix

1. Recréez votre [Atlas à l'usage des artistes et des militaires](https://www.google.be/search?q=atlas+broodthaers&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjO05OZtK3SAhVHbhQKHZUwDEoQsAQIGw&biw=2597&bih=1276)
1. Créez un monochrome dont la couleur est la couleur moyenne de tous les pixels d'une image donnée.

1. A l'aide des fonctions natives de Processing, et de ce que nous avons vu jusqu'à présent, concevez une Horloge qui nous indique l'heure ou tout autre élément nous permettant de nous situer dans la journée (mais lié à l'heure réelle). Trouvez ce qu'il vous faut dans la référence.

1. A partir des [poèmes](https://www.google.be/search?q=carl+andre+poems&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjk0N34ya3SAhUhDsAKHRvUAfsQsAQIIg&biw=2560&bih=1276#imgrc=tF8tCT3JrOAJfM:) de [Carl Andre](https://www.google.be/search?q=carl+andre+poems&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjk0N34ya3SAhUhDsAKHRvUAfsQsAQIIg&biw=2560&bih=1276#tbm=isch&q=carl+andre&*) (par exemple), créez une composition sur base de texte uniquement et exportez-la au formats PNG et PDF. http://socks-studio.com/2014/04/17/the-shape-of-poetry-carl-andres-typed-works/ 

1. Créez votre propre outil de dessin, votre propre brosse. Votre outil doit être contrôlé par votre souris (position) et clic (action) et 2 variables à l’aide du clavier (2 touches, par exemple couleur et épaisseur ou tout autre variables ayant un impact graphique sur votre canevas). Exploitez votre outil et exportez.

