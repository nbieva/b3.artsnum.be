#Les variables

l'on peut définir une variable comme un espace réservé dans la mémoire de votre ordinateur pour y stocker une valeur (d'un certain type). Cette valeur va pouvoir par la suite être utilisée, manipulée, modifiée, remplacée.
La position en X et en Y de votre souris sont deux variables. Déplacez votre souris et les valeurs de ces variables seront modifiées.

Dans Processing, il nous faut définir un type pour chaque variable créée. Ce n'est pas nécessaire en javaScript par exemple.
**Les principaux types** sont les suivants:

+ **int** : nombre entier (.. ,-2 ,-1 ,0 ,1 ,2 ,.. )
+ **float** : nombre à virgule flottante (0.5263, 452.18, 1.0, ..)
+ **string** : chaîne de caractères, à mettre entre guillemets ( "Peinture", "Photographie", ..)
+ **boolean** : variable booléenne. Sera vraie ou fausse (**true** or **false**)
+ **color** : sert à stocker une couleur

Vous en trouverez d'autres la [page suivante](part2/variables-2.md).

les différentes étapes dans l'utilisation d'une variable sont les suivantes:

+ On déclare la variable (ou les variables)
+ On lui assigne une valeur
+ On l'utilise

```processing
int monNombre;

void setup() {
    size(500,500);
}
void draw() {
    ellipse(mouseX, mouseY, 5, 5);
}
```
On peut aussi combiner les deux premières étapes comme suit:

```processing
int monNombre;

void setup() {
    size(500,500);
    monNombre = 100;
}
void draw() {
    ellipse(monNombre, monNombre, monNombre/2,  monNombre/2);
    // Dessinera un cercle de 50 sur 50 positionné en 100,100
}
```

Notez que:

+ On ne déclare le type qu'une seule fois dans le programme
+ Les noms de variables ne comprennent ni espaces, ni accents, ni chiffres, et commencent par une minuscule.
+ Certains noms de variables sont "réservés", par les variables natives notamment (mouseX, frameCount, ..)

![](/assets/variables-1.png)

###Incrémenter une variable

```processing
//Déclarez les deux variables pour les positions en X et Y de votre éllipse
int posX = 0;
int posY = 150;

void setup() {
  size(750, 300);
  noStroke(); // Pas de contour
  ellipseMode(CENTER);
}

void draw() {
    background(220);
    fill(255,0,0); //rouge
    ellipse(posX, posY, 100, 100);

    // On incrémente la position en X de 1 à chaque boucle
    posX = posX + 1; // Peut aussi s'écrire posX += 1;
    if (posX == 800) {
        posX = -50;
    }
}
```

<iframe  width="750" height="320" src="https://editor.p5js.org/embed/B12v2K8qQ"></iframe>

##Variables natives

Il existe dans Processing une série de variables natives telles que **mouseX** et **mouseY**.
Si j'utilise mouseX et mouseY dans mon sketch (à l'intérieur de la boucle draw en l'occurrence..), ces variables seront remplacées par la position en X et en Y de ma souris. Ainsi pour créer un simple outil de dessin, nous pouvons utiliser le code qui suit:

```processing
void setup() {
    size(500,500);
}
void draw() {
    ellipse(mouseX, mouseY, 5, 5);
}
```

Il existe d'autres variables natives dans processing, dont les plus importantes sont:

+ **width** : largeur de votre sketch
+ **height** : hauteur de votre sketch
+ **frameCount** : index de la boucle (sorte de compteur)
+ **pmouseX** : position précédente de la souris en X
+ **pmouseY** : position précédente de la souris en Y

Ainsi, on peut affiner notre sketch précédent comme suit:

```processing
void setup() {
    size(750,400);
}
void draw() {
    line(pmouseX, pmouseY, mouseX, mouseY);
}
```

<iframe width="760" height="410" src="https://editor.p5js.org/embed/SJaUSGCYm"></iframe>

On peut aussi lier une valeur (ici le diamètre du disque) à une autre variable (ici la position de la souris en Y) :

<iframe width="750" height="310" src="https://editor.p5js.org/embed/HJi0tzAKX"></iframe>


-----

Créez un sketch dans lequel varient un grand nombre de variables simultanément (background, hauteurs, largeurs, contours, couleurs, etc.)

##En savoir plus :

+ https://fr.flossmanuals.net/processing/les-variables/
