# Export au format PDF \(vectoriel\)

L'export en PDF nécessite l'importation d'une bibliothèque (fonctions supplémentaires) dans votre sketch. ce n'est pas bien compliqué. 

+ Ouvrez votre sketch
+ Cliquez sur **Sketch** > **Importer une librairie** > **Ajouter une librairie** (Si vous ne voyez pas PDF Export dans la liste)
+ Cherchez et choisissez **PDF Export** (processing foundation), puis cliquez sur **Install**. Fermer ensuite la boîte de dialogue.
+ Importez la librairie dans votre Sketch ( **Sketch** > **Importer une librairie** > Choix dans la liste )

Le PDF étant un format vectoriel, il aura l'avantage de pouvoir être exploité et retravaillé directement dans un logiciel de dessin vectoriel tels Illustrator ou Inkscape.

Les différentes façons d'enregistrer vos images sont détaillées, très clairement ici:

[https://processing.org:8443/reference/libraries/pdf/index.html](https://processing.org:8443/reference/libraries/pdf/index.html)

[https://processing.org/reference/libraries/pdf/](https://processing.org/reference/libraries/pdf/)

Voir aussi la section INPUT dans la [référence Processing](https://processing.org/reference/)

![](/assets/pdfcap.png)

```processing
import processing.pdf.*;

int x1;
int y1;
int x2;
int y2;
int diametre = 10;

void setup() {
    size(800,600);
    beginRecord(PDF, "lignes.pdf");
    strokeWeight(0.5);
}

void draw() {
    fill(random(255),random(255),random(255));
    x1 = round(random(15,width-15));
    y1 = round(random(15,height-15));
    x2 = round(random(15,width-15));
    y2 = round(random(15,height-15));

    stroke(0,180);// valeur de gris, opacité
    line(x1,y1,x2,y2);
    
    noStroke();
    ellipse(x1,y1,diametre,diametre);
    ellipse(x2,y2,diametre,diametre);
}
void keyPressed() {
    endRecord();
    stop();
}
```

Dans le code ci-dessus, les fonctions importantes concernant l'enregistrement de votre PDF sont les suivantes:
```processing
    import processing.pdf.*; //Importe la librairie dans votre sketch. A ajouter hors du setup()
    beginRecord(PDF, "lignes.pdf"); //Commence à enregistrer vos tracés à partir de cette ligne
    endRecord(); //Stoppe l'enregistrement et termine le fichier
```


---

Le fichier généré pourrait très bien être ensuite exploité dans Illustrator ou Inkscape...

![](/assets/capdf1.png)

![](/assets/capdf2.png)

![](/assets/capdf3.png)
