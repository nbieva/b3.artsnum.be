#Une grille de points

```processing
int x = 0;
int y = 0;

void setup() {
    size(800,600);
    background(255);
}

void draw(){
    fill(0);
    ellipse(x, y, 5, 5);
    if (x < width){
        x = x + 10;
    } else {
        x = 0;
        y = y + 10;
    }
}

```

![](/assets/points.png)

Vous pourriez assez facilement faire en sorte que l'ensemble de la trame soit dessinée à chaque boucle draw. Jetez un petit coup d'oeil [par ici!](https://code.artsnum.be/part2/boucles.html)

#Le même code, commenté:

```processing
int x = 0; // valeur initiale du positionnement horizontal
int y = 0; // valeur initiale du positionnement vertical

void setup() {
    // dimension de votre sketch
    size(800,600);
    // fond blanc
    background(255);
}

void draw(){
    fill(0); // fond noir
    ellipse(x, y, 5, 5); // on dessine le cercle avec pour position horizontale et verticale les variables x et y
    // si x est inférieur à la largeur du canvas, on l'incrémente de 10
    if (x < width){
        x = x + 10;
        // sinon, on le fait revenir à zéro et on incrémente de 10 y pour faire une nouvelle ligne
    } else {
        x = 0;
        y = y + 10;
    }
}

```
