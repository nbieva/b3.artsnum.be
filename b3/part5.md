#5. Datas

+ Formats json
+ Les objects
+ Data driven documents : https://d3js.org/
+ https://www.zooniverse.org/projects/zooniverse/floating-forests
+ https://www.quora.com/Where-can-I-find-public-or-free-real-time-or-streaming-data-sources
+ https://www.octoparse.com/blog/big-data-70-amazing-free-data-sources-you-should-know-for-2017?qu

+ A simple node package to convert iCal data (.ics file) to JSON format : https://github.com/adrianlee44/ical2json
+ Does the data exists : https://opendatabarometer.org/4thedition/detail-country/?_year=2016&indicator=ODB&detail=BEL

##Pixels array


```processing
PImage img;
float posx, posy, coteH, coteV, nbrH, nbrV;
color maCouleur;

void setup() {
  size(800,800);
  background(0);
  noStroke();
  posx = 0;
  posy = 0;
  nbrH = 800;
  nbrV = 800;
  coteH = width/nbrH;
  coteV  = height/nbrV;
  img = loadImage("earth800.jpg");
  img.loadPixels();
  //translate(0,30);
  for(int i=0; i<img.pixels.length;i++) {
    float r = red(img.pixels[i]);
    float v = green(img.pixels[i]);
    float b = blue(img.pixels[i]);
    float a = alpha(img.pixels[i]);
    /*if(r < 50 && v < 100 && b < 150) {
      
    } else {
      
    }*/
    fill(r,v,b,a);
      rect(posx, posy, coteH, coteV);
    if(posx >= width) {
      posx = 0;
      posy += coteV;
    } else if (posy > height) {
      noLoop();
    }
    posx += coteH;
  }
  img.updatePixels();
}
```