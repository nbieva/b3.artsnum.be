<p class="lead">Introduction aux tableaux (arrays) et à leur manipulation dans Processing ou P5js. Import et utilisation d'images dans un sketch, etc..</p>

#3. Tableaux et images
![Pixels](/assets/array.png)

###Il y a une semaine
+ L'**aléatoire** avec la fonction random()
+ Les **conditions**
+ Les **boucles for**
+ **Interactivité**
+ La **concaténation**

###Aujourd'hui
+ Exercice récapitulatif (voir ci-dessous)
+ Les **tableaux** (listes, ou arrays) + Exercice (voir ci-dessous)
+ Tableaux et boucles
+ Les images : **Charger et utiliser une image** dans un sketch (+ preload).
+ **Sauver** une image

###Les deux xr6 de ce jeudi 2 mai

####Recap1

+ Créez un sketch qui s'affichera en plein écran, avec un fond très sombre. 
+ A chaque clic de souris dans la page, une ellipse (disque) doit apparaître à l'emplacement du clic. 
+ Elle doit être d'un diametre entre 30 et 80 pixels et d'une couleur aléatoire mais plutôt jaune, et légèrement translucide. 
+ Toutes les ellipses doivent rester visibles.
+ Sous chaque ellipse doit être écrit en rouge (en petit mais lisible) le moment où le clic a eu lieu sous la forme HH:MM:SS (par exemple: 15:33:12)
+ Si la seconde au moment du clic est entre 30 et 60, l'ellipse doit avoir un contour blanc de 15px.
+ Si l'on appuye sur la touche "e", l'image doit être sauvée au format jpg avec un nom de fichier incluant l'heure, la minute et la seconde, à la manière d'une capture d'écran. (genre "monImage-15-33-14.jpg")

<iframe width="800" height="380" src="https://editor.p5js.org/nicobiev/embed/c8knJUEny" style="margin:3rem 0;border-radius:5px;"></iframe>

####Ex Tableaux

+ Créez 3 tableaux différents, avec une dizaine d'entrées chacun minimum. Un pour les sujets, un pour les verbes et un pour les compléments. (Vous pouvez aussi avoir des tableaux sujets singuliers, sujets pluriels, verbes singuliers, verbes pluriels etc...)
+ A chaque clic dans la page, une phrase complète, aléatoire, doit être écrite à l'endroit du clic.
+ Seule la phrase du dernier clic doit être visible.

###Emma, ton code:

```javascript
function setup() {
    createCanvas(windowWidth,windowHeight);
    background(10,0,0);
    stroke(0,30);
}
function draw() {
    star(width/2, height/2, 40, mouseY, mouseX);
}

function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
      let sx = x + cos(a) * radius2;
      let sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a + halfAngle) * radius1;
      sy = y + sin(a + halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }

function keyPressed() {
    if (hour()<12) {
        save("Capture d'étoile-"+hour()+"-"+minute()+"-"+second()+".jpg");
    }
}
```