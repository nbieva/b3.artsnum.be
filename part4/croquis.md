#Croquis/sketch (1)

Commencez par imaginer ce que vous aimeriez réaliser.
Commencez par quelque chose de simple, utilisant un registre de formes géométriques simples.
Vous pouvez créer un drapeau, une affiche, un logo.. La taille de votre premier sketch doit être de 900px par 600px.

+ Sur le template qui vous a été remis, tracez un projet
+ **Notez les coordonnées** de vos points
+ imaginez les valeurs de couleur à entrer
+ **pensez à l'ordre dans lequel vous allez dessiner les éléments**
+ Imaginez un élément qui doive suivre votre souris
+ Certaines de vos formes peuvent ne pas avoir de contour, d'autres oui.
+ Vous pouvez également jouer sur l'opacité ou les [modes de fusion](https://processing.org/reference/blendMode_.html)
+ N'hésitez pas à également utiliser des [formes libres](https://processing.org/reference/beginShape_.html).
+ [Exportez votre image au format PNG et/ou PDF](https://code.artsnum.be/part7/exports.html). (Si PDF, essayez de l'ouvrir dans Illustrator pour voir ce qu'il en est. Inspectez en mode tracés)

Pour avoir une idée des coordonnées de vos points, vous pouvez également utiliser le petit outil suivant : http://mousehover.net/coordonnees/


![ex0](/assets/ex0.jpg)

#### Les fonctions les plus utiles ici seront probablement :

+ fill() et noFill()
+ stroke() et noStroke()
+ strokeWeight
+ RectMode()
+ ellipseMode()
+ rect(), ellipse(), line(), point()
+ beginShape(), vertex(), endShape()
+ println()
+ save()

Utilisez le menu ci-contre, ou [la référence](https://processing.org/reference/) processing, pour trouver plus de détails à propos de ces fonctions.

Vous utiliserez déjà probablement les variables suivantes: width, height, frameCount, mouseX, mouseY