<p class="lead">Vous trouverez ci-dessous quelques pistes de travail pour la journée. Vous pouvez, comme nous l'avons dit, travailler à partir de codes que vous iriez chercher à droite à gauche.</p>

Veillez aussi à sauver et exporter votre travail.

Idées à creuser...

##Créer une horloge personnalisée

Une horloge qui n'est pas à envisager comme des aiguilles qui tournent, mais comme la visualisation du temps (de données, de quantités).

+ hour(), minute(), second(), variables, boucles, couleurs...

<iframe width="760" height="150" style="margin-top:30px;border-radius:3px;" src="https://editor.p5js.org/nicobiev/embed/cWneuXsNd"></iframe>

##Un outil de dessin

+ Notions de base
+ Interactivité avec la souris: mousePressed(), keyPressed(), ..

##Recréer une image et l'animer

+ Ikko Tanaka
+ Boucles, couleurs, variables

##Pixels sorting

+ Classer les pixels (pixel sorting)

##Aléatoire

+ Recréer une oeuvre de Sol Lewitt ou autre
+ Random, noise

##Créer un film animé

+ Menu > Tools > Movie Maker

##Créer une grille, un motif

+ Créer une fonction personnalisée + une boucle.

##Créer un dessin ou un personnage qui réagit au son

+ Microphone

##Slit scan personnalisé

+ Vidéo

##Créer une interface

+ Ajout de bibliothèque, ControlP5

##Manipuler du texte

+ Travailler avec les chaînes de caractères
+ text()
+ listes?

##Exporter votre travail vers Illustrator

Comment Processing pourrait-il être complémentaire d'Illustrator

##Travailler avec des données

+ Format json
