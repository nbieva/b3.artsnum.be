
+ Introduction générale
+ Introduction technique
+ Atelier : Prise en main et premier exercice (formes simples interactives)

+ Slit Scan
+ Point sur les besoins qui se sont fait sentir et réponses techniques
+ Atelier : Développement d'un projet personnel, par groupe, en fonction des codes et exemples présentés
+ Transfert des travaux
+ Echanges autour des travaux rendus
+ Conclusion
+ Formulaire